import React, {Component} from 'react';
import GooglyEyes from './components/googlyEyes'
import './App.css';

class App extends Component {

    constructor() {
        super();

        this.state = {
            toogle : false
        }
    }

    takePicture() {
        //console.log(this.refs.googlyEye)
        this.refs.googlyEye.picture();
    }

    render() {
        return (
            <div className="App">
                <button className={"menuButton"} onClick={() => this.setState({toggle : !this.state.toggle})}>burger</button>
                <div className={"menu " + (this.state.toggle ? 'open' : '')}></div>
                <button onClick={() => this.takePicture()}>PICTURE</button>
                <GooglyEyes ref={"googlyEye"}></GooglyEyes>
            </div>
        );
    }
}

export default App;
